import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {
  transform(value: string, todas: boolean = true): string {
    value = value.toLocaleLowerCase();
    if (todas) {
      const nombres = value.split(' ');
      nombres.forEach((nom, i) => {
        nombres[i] = nom[0].toUpperCase() + nom.substr(1);
      });
      return nombres.join(' ');
    }
    return value[0].toUpperCase() + value.substr(1);
  }
}
