import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'password'
})
export class PasswordPipe implements PipeTransform {

  transform(value: string, activar: boolean): any {
    if (!activar) {
      return value.replace(/./g, '*');
    }
    return value;
  }

}
