import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'buttonNombre'
})
export class ButtonNombrePipe implements PipeTransform {

  transform(value: string, activar: boolean): any {
    if (activar) {
      return 'Desactivar';
    }
    return 'Activar';
  }

}
